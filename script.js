var newgameBtn = document.getElementById('new-game-btn');

var show = function (e) {
    e.style.display = 'block';
}


const gameBoard = (function() {
    const gameBoardContainer = document.querySelector('.game-board-container');
    const drawGameBoard = function() {
        for (i = 0; i < 9; i++) {
            const gameBox = document.createElement('div');
            gameBox.className = 'game-box';
            gameBox.id = i;
            gameBox.addEventListener('click', () => {
                gamePlay.playTurn(parseInt(gameBox.id,10));
            });
            gameBoardContainer.appendChild(gameBox);
            newgameBtn.style.display = 'none';
        }
    }
    return {drawGameBoard};
})();

gameBoard.drawGameBoard();

const gameCell = document.getElementsByClassName('game-box');

const gamePlay = (function() {
    let gameCellsPlayed = [];
    let playerTurn = 1;
    let playerHasWon = false;
    const playTurn = function(score) {
        if (playerTurn === 1 && playerHasWon === false && !gameCellsPlayed.includes(score)) {
            playerOne.score.push(score);
            gameCellsPlayed.push(score);
            gamePlay.checkForWin(gamePlay.winConditions, playerOne.score, 'one')
            drawXO(gameCell[score]);
            return playerTurn = 2;
        }
        if (playerTurn === 2 && playerHasWon === false && !gameCellsPlayed.includes(score)) {
            playerTwo.score.push(score);
            gameCellsPlayed.push(score);
            gamePlay.checkForWin(gamePlay.winConditions, playerTwo.score, 'two')
            drawXO(gameCell[score]);
            return playerTurn = 1;
        }

    }
    const drawXO = function(gameBox) {
        const boardMarker = document.createElement('h1');
        if (playerTurn === 1) {
            boardMarker.innerHTML = 'X';
        } else {
            boardMarker.innerHTML = 'O';
        }
        gameBox.appendChild(boardMarker);
    }
    const winConditions = 
    [[0,1,2],
    [3,4,5],
    [6,7,8],
    [0,4,8],
    [2,4,6],
    [0,3,6],
    [1,4,7],
    [2,5,8]];

    function resetGame() {
        for (i = 0; i < 9; i++) {
            gameCell[i].innerHTML = '';
        }
        gameCellsPlayed = [];
        playerTurn = 1;
        playerHasWon = false;
        playerOne.score = [];
        playerTwo.score = [];
        newgameBtn.style.display = 'none';
    }

    function checkForWin(winConditions, playerScore, playerWinner) {
        for (i = 0; i < winConditions.length; i++) {
            if (arrayContainsArray(winConditions[i], playerScore) == true) {
                playerHasWon = true;
                // resetGame();
                newgameBtn.style.display = 'block';
                return console.log(`Player ${playerWinner} wins!`)
            }
        }
    }

    return { gameCellsPlayed, playTurn, checkForWin, winConditions, drawXO, resetGame};
})();

const playerOne = (function() {
    let score = [];
    return { score };
})();
const playerTwo = (function() {
    let score = [];
    return {score };
})();

function arrayContainsArray(arrayOne, arrayTwo) {
    return arrayOne.every(function(value) {
        return arrayTwo.indexOf(value) > -1;
    });
}

newgameBtn.addEventListener('click', () => {
    console.log('new-game');
    gamePlay.resetGame();
})



